import * as action from "./todoList";

describe('todo action', () => {
   it('add message in todo', () => {
       expect(action.addTodo('do homework')).toEqual({
           type: 'ADD_TODO',
           payload: {
               todo: {
                   id: 0,
                   name: 'do homework',
                   done: false,
                   edit: false
               }
           }
       });
   });

    it('toggle record in todo', () => {
        expect(action.toggleTodo(0)).toEqual({
            type: 'TOGGLE_TODO',
            payload: {
                todoId: 0
            }
        })
    });

    it('delete record in todo', () => {
        expect(action.deleteTodo(0)).toEqual({
            type: 'DELETE_TODO',
            payload: {
                todoId: 0
            }
        });
    });

    it('update edit mode', () => {
        expect(action.changeEditModeTodo(0)).toEqual({
            type: 'CHANGE_EDIT_MODE_TODO',
            payload: {
                todoId: 0
            }
        });
    });

    it('save update record in todo', () => {
        expect(action.saveChangesTodo(0, 'go to the shop')).toEqual({
            type: 'SAVE_CHANGES_TODO',
            payload: {
                todoId: 0,
                text: 'go to the shop'
            }
        });
    });
});
