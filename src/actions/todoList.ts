import TodoItem from "../models/todoItem";

export enum ActionTypes {
    ADD_TODO = "ADD_TODO",
    TOGGLE_TODO = "TOGGLE_TODO",
    DELETE_TODO = "DELETE_TODO",
    CHANGE_EDIT_MODE_TODO = "CHANGE_EDIT_MODE_TODO",
    SAVE_CHANGES_TODO = "SAVE_CHANGES_TODO",
    GET_TODO_LIST = "GET_TODO_LIST",
}

export interface AddTodoAction {
    type: ActionTypes.ADD_TODO, payload: { todo: TodoItem }
}

export interface ToggleTodoAction {
    type: ActionTypes.TOGGLE_TODO, payload: { todoId: number }
}

export interface DeleteTodoAction {
    type: ActionTypes.DELETE_TODO, payload: { todoId: number }
}

export interface ChangeEditModeTodoAction {
    type: ActionTypes.CHANGE_EDIT_MODE_TODO, payload: { todoId: number }
}

export interface SaveChangesTodoAction {
    type: ActionTypes.SAVE_CHANGES_TODO, payload: { todoId: number, text: string }
}

export interface GetTodoListAction {
    type: ActionTypes.GET_TODO_LIST, payload: { todoList: TodoItem[] }
}

export function addTodo(name: string): AddTodoAction {
    return {
        type: ActionTypes.ADD_TODO,
        payload: {
            todo: {
                id: 0,
                name: name,
                done: false,
                edit: false
            }
        }
    }
}

export function toggleTodo(id: number): ToggleTodoAction {
    return {
        type: ActionTypes.TOGGLE_TODO,
        payload: {
            todoId: id
        }
    }
}

export function deleteTodo(id: number): DeleteTodoAction {
    return {
        type: ActionTypes.DELETE_TODO,
        payload: {
            todoId: id
        }
    }
}

export function changeEditModeTodo(id: number) {
    return {
        type: ActionTypes.CHANGE_EDIT_MODE_TODO,
        payload: {
            todoId: id
        }
    }
}

export function saveChangesTodo(id: number, text: string) {
    return {
        type: ActionTypes.SAVE_CHANGES_TODO,
        payload: {
            todoId: id,
            text: text
        }
    }
}

export function getToDoList(todoList: TodoItem[]) {
    return {
        type: ActionTypes.GET_TODO_LIST,
        payload: {
            todoLists: todoList
        }
    }
}

export type Action = AddTodoAction | ToggleTodoAction | DeleteTodoAction | ChangeEditModeTodoAction | SaveChangesTodoAction | GetTodoListAction;
