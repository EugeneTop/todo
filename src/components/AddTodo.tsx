import React from "react";
import {TextField, Button} from "@material-ui/core";
import '../styles/AddTodo.css';
import {addTodo} from "../actions/todoList";
import {connect} from "react-redux";

interface Props {
    handleSubmit: (value: string) => void;
}

interface State {
    value: string;
}

export class AddTodo extends React.Component<Props, State> {
    public constructor(props: Props) {
        super(props);
        this.state = { value: '' };
    }

    private addTodo() {
        const message = this.state.value.trim();
        if (!message) {
            return
        }

        this.props.handleSubmit(message);
        this.setState({ value: '' });
    }

    private updateValue = (value: string) => {
        this.setState({ value });
    };

    private addTodoMessage = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        this.addTodo();
    };

    private clearField = () => {
        if (this.state.value === '') {
            return
        }

        this.setState({ value: '' })
    };

    private addMessageInTodoOnEnter = (key: string) => {
        if(key === 'Enter'){
            this.addTodo();
        }
    };

    public render () {
        const { value } = this.state;
        const { updateValue, addTodoMessage, clearField, addMessageInTodoOnEnter } = this;

        return (
            <div>
                <div className="container_add_todo">
                    <div className="container_input_todo">
                        <TextField className="input_todo_list" value={value} onKeyPress={e => addMessageInTodoOnEnter(e.key)}
                                   onChange={e => updateValue(e.target.value)} />
                    </div>
                    <div className="container_forButton_todo">
                        <Button className="button_todo_list submit" onClick={addTodoMessage} type="submit" variant="contained" color="primary">
                            Добавить
                        </Button>
                    </div>
                    <div className="container_forButton_todo">
                        <Button className="button_todo_list cancel" onClick={clearField} type="button" variant="contained" color="secondary">
                            Очистить
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = {
    handleSubmit: addTodo
};

export default connect(null, mapDispatchToProps)(AddTodo);

