import TodoItem from "../models/todoItem";
import React from "react";
import '../styles/Todo.css';
import EditableString from "./EditableString";
import TodoWithEdit from "./NotEditableString";
import {changeEditModeTodo, deleteTodo, getToDoList, saveChangesTodo, toggleTodo} from "../actions/todoList";
import {connect} from "react-redux";

interface Props {
    todoList: TodoItem[];
    onTodoClicked: (todoId: number) => void;
    onDeleteTodo: (todoId: number) => void;
    onChangeEditMode: (todoId: number) => void;
    onSaveChanges: (todoId: number, text: string) => void;
    setTodoList: (todoList: TodoItem[]) => void;
}

const TODO_LIST_NAME_IN_STORAGE = "todoList";

export class TodoList extends React.Component<Props> {

    public componentDidMount() {
        const todoListJSON = localStorage.getItem(TODO_LIST_NAME_IN_STORAGE);

        if (todoListJSON !== null) {
            const todoList = JSON.parse(todoListJSON);
            this.props.setTodoList(todoList);
        }
    }

    public componentDidUpdate() {
        localStorage.setItem(TODO_LIST_NAME_IN_STORAGE, JSON.stringify(this.props.todoList));
    }

    private shutdownEditChange() {
        this.props.todoList.forEach((todo) => {
            if(!todo.edit) {
                return;
            }

            this.props.onChangeEditMode(todo.id);
        });
    }

    private changeEditMode = (id: number) => {
        this.shutdownEditChange();
        this.props.onChangeEditMode(id);
    };

    private renderRecord = (item: TodoItem) => {
        const { onSaveChanges, onDeleteTodo, onTodoClicked } = this.props;
        const { changeEditMode } = this;

        if(item.edit) {
            return <TodoWithEdit key={item.id} todo={item} onSaveChanges={onSaveChanges}/>
        }

        return <EditableString key={item.id} todo={item} onDeleteTodo={onDeleteTodo}
                               onTodoClicked={onTodoClicked} onChangeEditMode={changeEditMode}/>
    };

    private renderTodoList = () => {
        const {todoList} = this.props;
        return todoList.map((item) => this.renderRecord(item));
    };

    public render() {
        const { renderTodoList } = this;

        return (
            <div className="container_todo_lists">
                {renderTodoList()}
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    todoList: state.todoList.todoList
});

const mapDispatchToProps = {
    onTodoClicked: toggleTodo,
    onDeleteTodo: deleteTodo,
    onChangeEditMode: changeEditModeTodo,
    onSaveChanges: saveChangesTodo,
    setTodoList: getToDoList,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
