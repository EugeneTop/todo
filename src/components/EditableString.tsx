import TodoItem from "../models/todoItem";
import React from "react";
import {Button, Checkbox, FormControlLabel} from "@material-ui/core";
import '../styles/Todo.css';

interface Props {
    todo: TodoItem;
    onDeleteTodo: (todoId: number) => void;
    onTodoClicked: (todoId: number) => void;
    onChangeEditMode: (todoId: number) => void;
}

export default class EditableString extends React.Component<Props>{

    public render() {
        const { todo, onDeleteTodo, onTodoClicked, onChangeEditMode } = this.props;

        return (
            <div className="container_row_todo">
                <div className="container_for_text_todo">{todo.name}</div>
                <div className="container_for_buttons">
                    <Button className="button_todo" type="button" variant="contained" onClick={() => onDeleteTodo(todo.id)} color="secondary">
                        Удалить
                    </Button>
                </div>
                <div className="container_for_buttons">
                    <Button className="button_todo" type="button" variant="contained" color="primary" onClick={() => onChangeEditMode(todo.id)}>
                        Изменить
                    </Button>
                </div>
                <div className="container_for_buttons">
                    <FormControlLabel
                        control={
                            <Checkbox checked={todo.done} onChange={() => onTodoClicked(todo.id)} value="checkedA"/>
                        }
                        label="Выполнено"
                    />
                </div>
            </div>
        );
    }
}
