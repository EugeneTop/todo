import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from "react";
import sinon from 'sinon';
import { AddTodo } from '../components/AddTodo';

Enzyme.configure({ adapter: new Adapter() });
const createTodo = jest.fn();

function shallowSetup() {
    const props = {
        handleSubmit: createTodo
    };

    const enzymeWrapper: Enzyme.ShallowWrapper<{handleSubmit: (value: string) => void;}, {value: string;}> = shallow(<AddTodo {...props} />);

    return {
        props,
        enzymeWrapper
    }
}

describe("Shallow rendered component AddTodo", () => {
    let wrapper: Enzyme.ShallowWrapper<{handleSubmit: (value: string) => void;}, {value: string;}>, sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        Object.getOwnPropertyNames(AddTodo.prototype).forEach((method: any) => {
            sandbox.spy(AddTodo.prototype, method);
        });
        const { enzymeWrapper} = shallowSetup();
        wrapper = enzymeWrapper;
    });

    afterEach(() => {
        sandbox.restore();
        createTodo.mockClear();
    });

    it('should check on exist buttons and edit', function () {
        expect(wrapper.find('.button_todo_list').length).toBe(2);
        expect(wrapper.find('.input_todo_list').length).toBe(1);
        expect(wrapper.find('.submit').length).toBe(1);
        expect(wrapper.find('.cancel').length).toBe(1);
    });

    describe('Creating todo list', () => {

       it('should work event on add new record in todo list', () => {
           const todoInput = wrapper.find('.input_todo_list');
           todoInput.simulate('change', {
               target: {
                   value: 'do homework'
               }
           });
           expect(wrapper.state().value).toBe('do homework');
           const buttonSubmit = wrapper.find('.submit');
           buttonSubmit.simulate('click', {
               preventDefault: () => {}
           });
       });


    });

    describe('should reset input for task todo', () => {

        it('should work event on add new record in todo list', () => {
            const todoInput = wrapper.find('.input_todo_list');
            todoInput.simulate('change', {
                target: {
                    value: 'do homework'
                }
            });
            expect(wrapper.state().value).toBe('do homework');
            const buttonSubmit = wrapper.find('.cancel');
            buttonSubmit.simulate('click', {
                preventDefault: () => {}
            });
            expect(wrapper.state().value).toBe('');
        });


    });
});
