import TodoItem from "../models/todoItem";
import {Action, ActionTypes} from "../actions/todoList";

export interface State {
    todoList: TodoItem[]
}

export const initialState: State = {
      todoList: []
};

export function todoList(state: State = initialState, action: Action) {
    switch (action.type) {

        case ActionTypes.ADD_TODO: {
            const todoList = action.payload.todo;
            todoList.id = state.todoList.length;

            return {
                ...state,
                todoLists: [...state.todoList, todoList]
            }
        }

        case ActionTypes.TOGGLE_TODO: {
            const todoId = action.payload.todoId;

            return {
                ...state,
                todoLists: state.todoList.map(todo => todo.id === todoId ? {...todo, done: !todo.done} : todo)
            }
        }

        case ActionTypes.DELETE_TODO: {
            const todoId = action.payload.todoId;
            const todoLists = state.todoList.filter(todo => todo.id !== todoId);
            todoLists.forEach((element, index) => {
                element.id = index;
            });

            return {
                ...state,
                todoLists: todoLists
            }
        }

        case ActionTypes.CHANGE_EDIT_MODE_TODO: {
            const todoId = action.payload.todoId;

            return {
                ...state,
                todoLists: state.todoList.map(todo => todo.id === todoId ? {...todo, edit: !todo.edit} : todo)
            }
        }

        case ActionTypes.SAVE_CHANGES_TODO: {
            const todoId = action.payload.todoId;
            const todoText = action.payload.text;

            return {
                ...state,
                todoLists: state.todoList.map(todo => todo.id === todoId ? {...todo, edit: !todo.edit, name: todoText} : todo)
            }
        }

        case ActionTypes.GET_TODO_LIST: {
            const todoList = action.payload.todoList;
            console.log(todoList);
            return {
                ...state,
                todoLists: todoList
            };

        }

        default: {
            return state;
        }
    }
}
