export default interface TodoItem {
    id: number,
    name: string,
    done: boolean,
    edit: boolean,
}
